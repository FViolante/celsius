package sheridan;

public class Celsius {
	
	public static int fromFahrenheit(int deg) {
		
		int c = (int) Math.round((deg - 32) * 0.5556);
		
		
		return c;
	}
	
}
