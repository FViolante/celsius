package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class CelsiusTest extends Celsius {
	
	@Test
	public void testFromFahrenheit() {
		int degC = Celsius.fromFahrenheit(100);
		System.out.print(degC);
		assertTrue("Doesn't match result", degC == 38);
	}
	
	@Test
	public void testFromFahrenheitBoundaryIn() {
		int degC = Celsius.fromFahrenheit(-1);
		assertTrue("Doesn't match result", degC == -18);
	}
	
	@Test
	public void testFromFahrenheitBoundaryOut() {
		int degC = Celsius.fromFahrenheit(0);
		assertFalse("Doesn't match result", degC == -17);
	}
	
	@Test
	public void testFromFahrenheitException() {
		int degC = Celsius.fromFahrenheit(7);
		assertFalse("Doesn't match result", degC == 13);
	}

}
